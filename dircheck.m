function dircheck(dirname, display_text)
if nargin < 2, display_text = 1; end
 
if exist(dirname, 'dir') ~= 7; 
    mkdir(dirname); 
    Status = 'created';
else
    Status = 'exists';
end

if display_text
    %a.dW(['Dir ' Status ': ' dirname]);
end
