function pr = progress(x, y, do_toc)
% x  step
% y  total number of steps

if nargin < 3, do_toc = 0; end

p = x./y;

if(isequal(fix(p), p))

    %disp([int2str(p) '%'])
    fprintf('%4.0d %% ',p)
    
    % show time
    switch do_toc
        case 1
            toc;
        otherwise
    end
end

pr = p;

%x = 10; y=10;do_toc=0;
%progress(100,10,0);
